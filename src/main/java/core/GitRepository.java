package core;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class GitRepository {

    private String repoPath;

    public GitRepository(String repoPath) {
        this.repoPath = repoPath;
    }

    public String getHeadRef() {
        String line = "";

        try {
            Process p = Runtime.getRuntime().exec("cat " + repoPath + "/HEAD");
            p.waitFor();

            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

            line = reader.readLine().replace("ref: ", "");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return line;
    }

}